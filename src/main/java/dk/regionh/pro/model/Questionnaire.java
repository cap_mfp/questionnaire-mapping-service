package dk.regionh.pro.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@ApiModel(description="Epic questionnaire with corresponding mapping to QFDD")
public class Questionnaire {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String qfddId;

	private String epicId;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "questionnaire_question",
			joinColumns = @JoinColumn(name = "question_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "questionnaire_id", referencedColumnName = "id"))
	private Set<Question> questions;

	public Questionnaire(){
	    super();
    }

	public Questionnaire(String qfddId, String epicId, Question... questions) {
		this.qfddId = qfddId;
		this.epicId = epicId;

		this.questions = Stream.of(questions).collect(Collectors.toSet());
		this.questions.forEach(x -> x.getQuestions().add(this));

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQfddId() {
		return qfddId;
	}

	public void setQfddId(String qfddId) {
		this.qfddId = qfddId;
	}

	public String getEpicId() {
		return epicId;
	}

	public void setEpicId(String epicId) {
		this.epicId = epicId;
	}
}
