package dk.regionh.pro.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import java.util.*;

@Entity
@ApiModel(description="Mapping between Epic and QFDD question")
public class Question {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String qfddId;

	private String epicId;

	@ManyToMany(mappedBy = "questions")
	private Set<Questionnaire> questions = new HashSet<>();

	public Question(){
	    super();
    }

	public Question(String qfddId, String epicId) {
		this.qfddId = qfddId;
		this.epicId = epicId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQfddId() {
		return qfddId;
	}

	public void setQfddId(String qfddId) {
		this.qfddId = qfddId;
	}

	public String getEpicId() {
		return epicId;
	}

	public void setEpicId(String epicId) {
		this.epicId = epicId;
	}

	public Set<Questionnaire> getQuestions() {
		return questions;
	}

	public void setQuestions(Set<Questionnaire> questions) {
		this.questions = questions;
	}
}
