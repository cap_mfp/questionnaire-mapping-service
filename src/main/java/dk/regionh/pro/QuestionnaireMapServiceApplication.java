package dk.regionh.pro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestionnaireMapServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuestionnaireMapServiceApplication.class, args);
	}
}
