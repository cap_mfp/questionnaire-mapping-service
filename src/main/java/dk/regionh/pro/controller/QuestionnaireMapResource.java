package dk.regionh.pro.controller;

import dk.regionh.pro.model.Questionnaire;
import dk.regionh.pro.student.QuestionnaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
public class QuestionnaireMapResource {

	@Autowired
	private QuestionnaireRepository questionnaireRepository;

	@GetMapping("/mapping")
	public List<Questionnaire> retrieveAllQuestionnaireMaps() {
		return questionnaireRepository.findAll();
	}


	@PostMapping("/mapping")
	public ResponseEntity<Object> createStudent(@RequestBody Questionnaire questionnaireMap) {
		Questionnaire savedQmap = questionnaireRepository.save(questionnaireMap);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedQmap.getId()).toUri();

		return ResponseEntity.created(location).build();

	}
}
