



Notes on DB design

Epic Questions can be reused in different Questionnaires -
we assume that this is the case for QFDD as well. Therefore,
We try to enforce reuse of questionmapping. For this we need the QuestionMapping
for each Epic Question Id -> QFDD Question code (instead of a simple Map in each QuestionnaireMapping)
Example: If you're creating a new Questionnaire with questions that
already exists, you should not have to enter the mappings again, they should
already be in the database.